<?php
/**
 * Template Name: Podstránka 3.úrovně
 *
 * @package WordPress
 */
?>
<?php get_header(); ?>

<?php /*
<script type="text/javascript" 
	src="<?php echo get_template_directory_uri(); ?>/js/custom.content.scroll.js"></script>
*/?>
<?php
$permalink = get_permalink($post->post_parent);
$back_link = '<div class="back_link"><a href="'.$permalink.'">&#8617; zpět</a></div>';
?>
<div id="content" class="row">

	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main" style="width:100%">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php simple_boostrap_display_page(false);?>
		
		<?php echo $back_link; ?>
		
		<?php //comments_template('',true); ?>
		
		<?php endwhile; ?>		
		
		<?php else : ?>
		<article id="post-not-found" class="block">
			<div class="article-header">
				<h2><?php _e("Žádný obsah", "simple-bootstrap"); ?></h2>
			</div>
			<p><?php _e("Nebyly nalezeny žádné stránky.", "simple-bootstrap"); ?></p>			
		</article>
		
		<?php endif; ?>

	</div>

	
	<?php //get_sidebar("left"); ?>

</div>

<?php get_footer(); ?>
