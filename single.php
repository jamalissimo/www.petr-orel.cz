<?php get_header(); ?>

<?php /*
<script type="text/javascript" 
	src="<?php echo get_template_directory_uri(); ?>/js/custom.content.scroll.js"></script>
*/?>
<div id="content" class="row">
<?php 
  $url = $_SERVER["REQUEST_URI"];
  if(strpos($url, "aktuality")===false){
    get_sidebar("aktuality"); 
    $style = '';
    $back_link = '';
  }else{
    $style = 'style="width:100%"';
    $permalink = site_url()."/".get_the_date( "Y", $post->ID )."/";
    $back_link = '<div class="back_link"><a href="'.$permalink.'">&#8617; zpět</a></div>';
  }
?>
	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main" <?php echo $style;?>>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php simple_boostrap_display_page(false); ?>
		
		<?php echo $back_link; ?>
		
		<?php 
		/*
		comments_template('',true); ?>
		
		<?php if (get_next_post() || get_previous_post()) { ?>
		<nav class="block">
			<ul class="pager pager-unspaced">
				<li class="previous"><?php previous_post_link('%link', "&laquo; " . __( 'Previous Post', "simple-bootstrap")); ?></li>
				<li class="next"><?php next_post_link('%link', __( 'Next Post', "simple-bootstrap") . " &raquo;"); ?></li>
			</ul>
		</nav>
		
		<?php
		
		}*/ ?>
		
		<?php endwhile; ?>			
		
		<?php else : ?>
		
		<article id="post-not-found" class="block">
		    <p><?php _e("No posts found.", "simple-bootstrap"); ?></p>
		</article>
		
		<?php endif; ?>

	</div>

</div>

<?php get_footer(); ?>