<div class="container">
	<!-- responsive menu -->
	<div class="navbar-header button-nav-responsive">
		<?php if (has_nav_menu("main_nav")): ?>
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-responsive-collapse">
			<span class="sr-only"><?php _e('Menu', 'simple-bootstrap'); ?></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<?php endif ?>
		<!--
		<a class="navbar-brand" title="<?php //bloginfo('description'); ?>" href="<?php //echo esc_url(home_url('/')); ?>"><?php //bloginfo('name'); ?></a>
		-->
	</div>
	
	<!-- classic menu -->
	<?php if (has_nav_menu("main_nav")): 
	?>
	<div id="navbar-responsive-collapse" class="collapse navbar-collapse " >
		<?php
		simple_bootstrap_display_main_menu();
		?>
	</div>
	<?php endif ?>
</div>