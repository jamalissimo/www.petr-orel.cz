<div id="sidebar-left" class="<?php simple_boostrap_sidebar_left_classes(); ?>" role="complementary">
    <div class="vertical-nav block">
		<?php 
			$this_category = $this_category = get_category($cat);
			$current_cat_id = $cat;
			$child_of = $this_category->category_parent==0 ? $current_cat_id : $this_category->category_parent;
			
			/* debug */
			echo "<br>";
			echo "<br>";
			echo "parent alt - ".$this_category->category_parent;
			echo "<br>";
			echo "current alt alt - ".$cat;
			echo "<br>";
			/**/
			
			if (!empty($child_of)){
			      $args = array(
				  'show_option_all'    => '',
				  'orderby'            => 'name',
				  'order'              => 'ASC',
				  'style'              => 'list',
				  'show_count'         => 0,
				  'hide_empty'         => 1,
				  'use_desc_for_title' => 0,
				  'child_of'           => $child_of,
				  'feed'               => '',
				  'feed_type'          => '',
				  'feed_image'         => '',
				  'exclude'            => '',
				  'exclude_tree'       => '',
				  'include'            => '',
				  'hierarchical'       => 1,
				  'title_li'           => '',
				  'show_option_none'   => __( '' ),
				  'number'             => null,
				  'echo'               => 1,
				  'depth'              => 2,
				  'current_category'   => $current_cat_id,
				  'pad_counts'         => 0,
				  'taxonomy'           => 'category',
				  'walker'             => null
			      );

				echo '<div id="nav_menu_category-'.$current_cat_id.'" class="widget widget_nav_menu">';
				echo '<ul id="podstranky" class="menu">';
				  wp_list_categories( $args );
				echo "</ul>";
				
	
				echo "</div>";	
				wp_reset_postdata();
			}
		?>
    </div>
</div>