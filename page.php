<?php get_header(); ?>

<?php /*
<script type="text/javascript" 
	src="<?php echo get_template_directory_uri(); ?>/js/custom.content.scroll.js"></script>
*/?>
<div id="content" class="row">

	<?php get_sidebar("left"); ?>
	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php 
		    $depth = get_page_depth();
		    $children = get_pages('child_of='.$post->ID);
		    if(count( $children ) > 0 && $depth>0){
		      foreach($children as $post){
			simple_boostrap_display_page(true, $post->ID);
		      }
		    }else{
		      simple_boostrap_display_page(false);
		    }
		    if(count( $children )==0){
		      $back_link = '<div class="back_link"><a href="javascript:;" onclick="javascript:history.back(-1)">&#8617; zpět</a></div>';
		      echo $back_link;
		    }
		?>
		
		<?php //comments_template('',true); ?>
		
		<?php endwhile; ?>		
		
		<?php else : ?>
		<article id="post-not-found" class="block">
			<div class="article-header">
				<h2><?php _e("Žádný obsah", "simple-bootstrap"); ?></h2>
			</div>
			<p><?php _e("Nebyly nalezeny žádné stránky.", "simple-bootstrap"); ?></p>			
		</article>
		
		<?php endif; ?>

	</div>

	
	<?php //get_sidebar("left"); ?>

</div>

<?php get_footer(); ?>
