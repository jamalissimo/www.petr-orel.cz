<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<?php wp_head(); ?>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/style-custom.css' type='text/css' media='all' />
	
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId : '979612672131542',
					xfbml : true,
					version : 'v2.5'
				});
			};
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {
						return;
					}
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>
	</head>

	<body <?php body_class(); ?>>
		<div id="content-wrapper">
			<header>
				<!-- HOMEPAGE HEADER -->
				<div class="navbar navbar-default navbar-static-top green-header-container white_delimiter">
					<!-- FLOWER FOIL -->
					<!-- <div id="flower-foil"></div> -->
					<img src="<?php echo get_template_directory_uri(); ?>/images/kapradi.png" class="flower-foil" />
					<!-- /FLOWER FOIL -->

					<div class="header-top">
						<?php get_search_form('search-form'); ?>
						<?php
						include 'header-top.php';
						?>
					</div>
					<div class="header-nav">
						<?php
						include 'nav.php';
						?>
					</div>
				</div>
				<!-- /HOMEPAGE HEADER -->
			</header>
			<div id="page-content">
				<!-- SLIDER -->
				<?php echo display_slider_or_thumb(305); ?>
				<!-- /SLIDER -->
				<div class="container">
