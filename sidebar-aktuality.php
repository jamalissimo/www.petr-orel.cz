<div id="sidebar-left" class="<?php simple_boostrap_sidebar_left_classes(); ?>" role="complementary">
  <div class="vertical-nav block">
	<?php 
	$args = array(
		'type'            => 'yearly',
		'limit'           => '10',
		'format'          => 'html', 
		'before'          => '',
		'after'           => '',
		'show_post_count' => false,
		'echo'            => 1,
		'order'           => 'DESC',
		'post_type'       => 'post'
	);		
	
	echo '<div id="nav_menu_category-aktuality" class="widget widget_nav_menu">';
	echo '<ul id="podstranky" class="menu">';
	  wp_get_archives( $args );
	echo "</ul>";
	echo "</div>";		
	?>
	<?php get_sidebar("darujme"); ?>
	</div>
</div>