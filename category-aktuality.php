<?php
get_header(); ?>

<div id="content" class="row">

	<?php get_sidebar("aktuality"); ?>

	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">

		<?php 		
		$count_posts = get_posts_count();
		$posts = query_posts( $query_string . '&posts_per_page=8&orderby=date&order=desc&year='.date("Y") );
		if (have_posts()) : while (have_posts()) : the_post(); ?>		
		<?php		  
		  $multiple = $count_posts == 1 ? false : true;
		  simple_boostrap_display_page($multiple,"", true);
		?>		
		<?php endwhile; ?>		
		<?php //odkazy nefungjí ?>
		<?php //the_posts_pagination( array( 'mid_size' => 2 ) ); ?>
		
		<?php else : ?>
		
		<article id="post-not-found" class="block">
			<div class="article-header">
				<h2><?php _e("Žádný obsah", "simple-bootstrap"); ?></h2>
			</div>
			<p><?php _e("Nebyly nalezeny žádné stránky.", "simple-bootstrap"); ?></p>			
		</article>
		
		<?php endif; ?>

	</div>

	
	<?php //get_sidebar("right"); ?>

</div>

<?php get_footer(); 
?>
