
</div>
</div>
<!-- FOOTER NAV -->
<div class="footer-nav ">
	<div class="col-lg-12 white_delimiter">
       	 <?php include 'footer-nav.php';?>
    </div>
</div>
<div class="clearfix"></div>
<!-- ¨/FOOTER NAV -->
<footer>
	<div id="inner-footer" class="vertical-nav">
		<div class="col-lg-12 text-center">
			  <?php
			    global $nggdb;
			    $gallery = $nggdb->get_gallery ( 1, 'filename', 'ASC', true, 0, 0 );
			    foreach ( $gallery as $image ) {
			    ?>
			    <a class="footer-link" href="<?php echo $image->description;?>"
			      target="_blank"> <img src="<?php echo $image->imageURL;?>"
			      height="100" alt="<?php echo $image->alttext;?>"
			      title="<?php echo $image->description;?>">
			    </a>
			  <?php }?>
		</div>
		<div class="col-lg-12 white_delimiter copyright text-center">
			<p>
			      <?php _e("© ZÁKLADNÍ ORGANIZACE ČESKÉHO SVAZU OCHRÁNCŮ PŘÍRODY NOVÝ JIČÍN","simple-bootstrap"); ?>
			      | Designed by <a href="http://www.ateliersamaj.cz" target="_blank">Atelier Šamaj</a> | Webdesign <a href="http://www.janulek.cz" target="_blank">Roman Janulek</a> | 
			      <a href="<?php echo admin_url();?>">Správa</a>
						
				</p>
		</div>
		<?php
		if(!wp_is_mobile()){
		?>
		<div id="footer-image" class="col-lg-12">
		  <img class="img-responsive"
		    src="<?php echo get_template_directory_uri(); ?>/images/footer_bottom_overlay.png"
		    alt="paticka obrazek text"
		    title="Nikdy nezplatíme zvířatům co jsme jim dlužili"> 
		    <p class="footer_text">
		      Ing. Josef Vágner<br>
		      zakladatel ZOO Safari ve Dvoře Králové nad Labem
		    </p>
		</div>
		<?php }?>
	</div>
</footer>
</div>

<?php
if(!wp_is_mobile()){
?>
  <div style="height:600px;width:100%"></div>
<?php }?>

<?php wp_footer(); ?>
<!-- add custom js -->
<script type="text/javascript"
	src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script type="text/javascript" 
	src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.js"></script>	
</body>

</html>