
<?php if ( is_active_sidebar( 'sidebar-left' ) ) { ?>
<div id="sidebar-left" class="<?php simple_boostrap_sidebar_left_classes(); ?>" role="complementary">
    <div class="vertical-nav block">
	<?php 
	  $children = get_pages('child_of='.$post->ID);
	  if( count( $children ) != 0 || $post->post_parent > 0 ) {
	    $ID = $post->post_parent > 0 ? $post->post_parent : $post->ID;
	    $args = array(
	    'child_of'     => $ID,
	    'depth'        => 1,
	    'echo'         => 1,
	    'post_type'    => 'page',
	    'post_status'  => 'publish',
	    'sort_column'  => 'menu_order',
	    'sort_order'   => 'ASC',	    
	    'title_li'     => '', 
	    'walker'       => new Walker_Page); 
	    
	    echo '<div id="nav_menu-'.$ID.'" class="widget widget_nav_menu">';
	    echo '<ul id="podstranky" class="menu">';
	      wp_list_pages( $args );
	    echo "</ul>";
	    echo "</div>";
	  }
	 ?>
	 <?php get_sidebar("darujme"); ?>
	 <div class="left-menu-images">
	  <img src="<?php echo get_template_directory_uri(); ?>/images/logo_menu_left1.png" 
	    alt="" title="">
	  <img src="<?php echo get_template_directory_uri(); ?>/images/logo_menu_left2.png" 
	    alt="" title="" class="float-right">
	  
	  <?php
	  // Pouze u stránky Dům přírody Poodří
	  if($ID==6){
	  ?>
	    <img src="<?php echo get_template_directory_uri(); ?>/images/logo_menu_left3.png" 
	      alt="" title="" class="float-right">
	  <?php
	  }
	  ?>
	 </div>
	 <div class="article_left">
	  <?php simple_boostrap_display_page_by_id(289);?>
	 </div>
	</div>
</div>
<?php } ?>
