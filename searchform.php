<div class="header-top-strip">
	<div class="container ">
		<p class="text-uppercase left">ZÁKLADNÍ ORGANIZACE ČESKÉHO SVAZU OCHRÁNCŮ PŘÍRODY NOVÝ JIČÍN</p>
		<div class="header-search-bar">
			<form action="<?php echo home_url('/'); ?>" method="get">
				<fieldset>
					<div class="input-group header-search-bar right">
						<input type="text" name="s" id="search"
							placeholder="<?php _e("Vyhledávání", "simple-bootstrap"); ?>"
							value="<?php the_search_query(); ?>"
							class="form-control text-uppercase text-right" /> <span
							class="input-group-btn">
							<button type="submit" class="btn btn-primary"><?php _e("", "simple-bootstrap"); ?></button>
						</span>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>