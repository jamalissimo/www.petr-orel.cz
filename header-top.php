<div class="container ">
	<div class="navbar-header header-title col-lg-12">
		<div class="header-title-box text-center col-lg-4">
			<h1>
				<a title="<?php bloginfo('name'); ?>"
					href="<?php echo esc_url(home_url('/')); ?>">Záchranná stanice</a>
			</h1>
		</div>
		<div class="header-logo-box text-center col-lg-4">
			<a title="<?php bloginfo('name'); ?>"
				href="<?php echo esc_url(home_url('/')); ?>"> <img
				class="header-logo img-responsive"
				src="<?php echo get_template_directory_uri(); ?>/images/logo.png"
				alt="Záchranná Stanice Bartošovice logo" />
			</a>
		</div>
		<div class="header-title-box text-center col-lg-4">
			<h2 class="light_orange">
				<a title="<?php bloginfo('description'); ?>"
					href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('description'); ?></a>
			</h2>
		</div>
	</div>
</div>