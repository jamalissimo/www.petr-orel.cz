<?php get_header(); ?>

<div id="content" class="row">
	<?php get_sidebar("aktuality"); ?>
		<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">
		
		<?php 
			$posts = query_posts( $query_string . '&posts_per_page=8&orderby=date&order=desc' );
			if ( have_posts() ) : 
		?>
			<header class="page-header">
			<?php
			  the_archive_title( '<h1 class="page-title">', '</h1>' );
			?>			
			<?php
			  the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
			</header><!-- .page-header -->
			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
				simple_boostrap_display_post(true,"",true);
			endwhile;
			the_posts_pagination( array( 'mid_size' => 2 ) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</div><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
