<?php

// Declaration of theme supported features
function simple_boostrap_theme_support() {
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    ));
    add_theme_support('post-thumbnails');      // wp thumbnails (sizes handled in functions.php)
    set_post_thumbnail_size(125, 125, true);   // default thumb size
    add_theme_support('automatic-feed-links'); // rss thingy
    add_theme_support('custom-background', array(
        'default-color' => '#595959',
    ));
    add_theme_support( 'title-tag' );
    register_nav_menus(                      // wp3+ menus
        array( 
            'main_nav' => __('Hlavní menu', 'simple-bootstrap'),   // main nav in header
            'second_nav' => __('Boční menu', 'simple-bootstrap'),   // main nav in header
        )
    );
    add_image_size( 'simple_boostrap_featured', 1140, 1140 * (9 / 21), true);
    load_theme_textdomain( 'simple-bootstrap', get_template_directory() . '/languages' );
}
add_action('after_setup_theme','simple_boostrap_theme_support');

function simple_bootstrap_theme_scripts() { 
    // For child themes
    wp_register_style( 'wpbs-style', get_stylesheet_directory_uri() . '/style.css', array(), null, 'all' );
    wp_enqueue_style( 'wpbs-style' );
    wp_register_script( 'bower-libs', 
        get_template_directory_uri() . '/js/app.min.js', 
        array('jquery'), 
        null );
    wp_enqueue_script('bower-libs');
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'simple_bootstrap_theme_scripts' );

function simple_bootstrap_load_fonts() {
    wp_register_style('simple_bootstrap_googleFonts', '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext');
    wp_enqueue_style('simple_bootstrap_googleFonts');
}

add_action('wp_print_styles', 'simple_bootstrap_load_fonts');

// Set content width
if ( ! isset( $content_width ) )
    $content_width = 750;

// Sidebar and Footer declaration
function simple_boostrap_register_sidebars() {
    register_sidebar(array(
        'id' => 'sidebar-right',
        'name' => __('Right Sidebar', 'simple-bootstrap'),
        'description' => __('Used on every page.', 'simple-bootstrap'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
    	'id' => 'sidebar-left',
    	'name' => __('Left Sidebar', 'simple-bootstrap'),
    	'description' => __('Used on every page.', 'simple-bootstrap'),
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));
    
    register_sidebar(array(
      'id' => 'footer1',
      'name' => __('Footer', 'simple-bootstrap'),
      'before_widget' => '<div id="%1$s" class="widget col-xs-6 col-sm-4 col-md-3 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));
    
}
add_action( 'widgets_init', 'simple_boostrap_register_sidebars' );

// Menu output mods
class simple_bootstrap_Bootstrap_walker extends Walker_Nav_Menu {

    function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0) {

        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $dropdown = $args->has_children && $depth == 0;

        $class_names = $value = '';

        // If the item has children, add the dropdown class for bootstrap
        if ( $dropdown ) {
            $class_names = "dropdown ";
        }

        $classes = empty( $object->classes ) ? array() : (array) $object->classes;

        $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

        if ( $dropdown ) {
            $attributes = ' href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"';
        } else {
            $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
            $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
            $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
            $attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';
        }

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
        $item_output .= $args->link_after;

        // if the item has children add the caret just before closing the anchor tag
        if ( $dropdown ) {
            $item_output .= ' <b class="caret"></b>';
        }
        $item_output .= '</a>';

        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
    } // end start_el function
    
    function start_lvl(&$output, $depth = 0, $args = Array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class='dropdown-menu' role='menu'>\n";
    }
    
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
function simple_bootstrap_add_active_class($classes, $item) {
    if( in_array('current-menu-item', $classes) ) {
        $classes[] = "active";
    }
  
    return $classes;
}
add_filter('nav_menu_css_class', 'simple_bootstrap_add_active_class', 10, 2 );

// display the main menu bootstrap-style
// this menu is limited to 2 levels (that's a bootstrap limitation)
function simple_bootstrap_display_main_menu() {
    wp_nav_menu(
        array( 
            'theme_location' => 'main_nav', /* where in the theme it's assigned */
            'menu' => 'main_nav', /* menu name */
            'menu_class' => 'nav navbar-nav text-uppercase',
            'container' => false, /* container class */
            'depth' => 1,
            'walker' => new simple_bootstrap_Bootstrap_walker(),
        )
    );
}
function wpb_first_and_last_menu_class($items) {
    $items[1]->classes[] = 'first';
    $items[count($items)]->classes[] = 'last';
    return $items;
}
add_filter('wp_nav_menu_objects', 'wpb_first_and_last_menu_class');

function simple_bootstrap_display_second_menu() {
    wp_nav_menu(
        array( 
            'theme_location' => 'second_nav', /* where in the theme it's assigned */
            'menu' => 'second_nav', /* menu name */
            'menu_class' => 'nav navbar-nav',
            'container' => false, /* container class */
            'depth' => 1,
            'walker' => new simple_bootstrap_Bootstrap_walker(),
        )
    );
}
/*
  A function used in multiple places to generate the metadata of a post.
*/
function simple_bootstrap_display_post_meta() {
?>

    <ul class="meta text-muted list-inline">
        <li>
            <a href="<?php the_permalink() ?>">
                <span class="glyphicon glyphicon-time"></span>
                <?php the_date(); ?>
            </a>
        </li>
        <li>
            <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>">
                <span class="glyphicon glyphicon-user"></span>
                <?php the_author(); ?>
            </a>
        </li>
        <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
        <li>
            <?php
                $sp = '<span class="glyphicon glyphicon-comment"></span> ';
                comments_popup_link($sp . __( 'Leave a comment', "simple-bootstrap"), $sp . __( '1 Comment', "simple-bootstrap"), $sp . __( '% Comments', "simple-bootstrap"));
            ?>
        </li>
        <?php endif; ?>
        <?php edit_post_link(__( 'Edit', "simple-bootstrap"), '<li><span class="glyphicon glyphicon-pencil"></span> ', '</li>'); ?>
    </ul>

<?php
}

function simple_boostrap_page_navi() {
    global $wp_query;

    ?>

    <?php if (get_next_posts_link() || get_previous_posts_link()) { ?>
        <nav class="block">
            <ul class="pager pager-unspaced">
                <li class="previous"><?php next_posts_link("&laquo; " . __('Older posts', "simple-bootstrap")); ?></li>
                <li class="next"><?php previous_posts_link(__('Newer posts', "simple-bootstrap") . " &rsquo;"); ?></li>
            </ul>
        </nav>
    <?php } ?>

    <?php
}

//použité pouze v search.php
function simple_boostrap_display_post($multiple_on_page, $post_id="", $post_date="") { ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class("block"); ?> role="main">
        
        <header>
            
            <?php if ($multiple_on_page) : ?>
            <div class="article-header">
                <h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            </div>
            <?php
            if($post_date!=""){
            ?>
            <div class="article-date">
	      <?php echo get_the_date("d.m.Y", get_the_ID()); ?>
	    </div>
            <?php
            }
            ?>
            <?php else: ?>
            <div class="article-header">
                <h2><?php the_title(); ?></h2>
            </div>            
            <?php endif ?>

            <?php
            /*
            if (has_post_thumbnail()) { ?>
            <div class="featured-image">
                <?php if ($multiple_on_page) : ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('simple_boostrap_featured'); ?></a>
                <?php else: ?>
                <?php the_post_thumbnail('simple_boostrap_featured'); ?>
                <?php endif ?>
            </div>
            <?php } 
            */?>

            <?php //simple_bootstrap_display_post_meta() ?>
        
        </header>
    
        <section class="post_content">
            <?php
            if ($multiple_on_page) {
								get_excerpt_by_id($post_id);
            } else {
                the_content();
                wp_link_pages();
            }
            ?>
        </section>
        
        <footer>
            <?php the_tags('<p class="tags">', ' ', '</p>'); ?>
        </footer>
    
    </article>

<?php }

function simple_boostrap_display_page($multiple_on_page, $post_id="", $post_date="") { ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class("block"); ?> role="article">
        
        <header>
            
            <?php if ($multiple_on_page) : ?>
            <div class="article-header article-multiple">
                <h2 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            </div>
	    <?php
            if($post_date!=""){
            ?>
            <div class="article-date">
	      <?php echo get_the_date("d.m.Y", get_the_ID()); ?>
	    </div>
            <?php
            }
            ?>            
            <?php else: ?>
            <div class="article-header">
                <h2><?php the_title(); ?></h2>
            </div>
            <?php endif ?>

            <?php
            /* if (has_post_thumbnail()) { ?>
            <div class="featured-image">
                <?php if ($multiple_on_page) : ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('simple_boostrap_featured'); ?></a>
                <?php else: ?>
                <?php the_post_thumbnail('simple_boostrap_featured'); ?>
                <?php endif ?>
            </div>
            <?php } */?>

            <?php //simple_bootstrap_display_post_meta() ?>
        
        </header>
    
        <section class="post_content">
            <?php
            if ($multiple_on_page) {
							get_excerpt_by_id($post_id);
            } else {
                the_content();
                wp_link_pages();
            }
            ?>
        </section>
        
    
    </article>

<?php }

function simple_boostrap_display_page_by_id($id) { 
  $page = get_post($id);
?>
    <article id="post-<?php echo $id; ?>" <?php post_class("block"); ?> role="article">
        <header>
            <div class="article-header">
                <h2><?php echo $page->post_title; ?></h2>
            </div>
        </header>
    
        <section class="post_content">
            <?php
	      $content = $page->post_content;
	      $content = apply_filters('the_content', $content);
	      echo $content;
            ?>
        </section>
    </article>

<?php }

function simple_boostrap_main_classes() {
    $nbr_sidebars = (is_active_sidebar('sidebar-left') ? 1 : 0) + (is_active_sidebar('sidebar-right') ? 1 : 0);
    $classes = "";
    if ($nbr_sidebars == 0) {
        $classes .= "col-sm-8 col-md-push-2";
    } else if ($nbr_sidebars == 1) {
        $classes .= "col-md-8";
    } else {
        $classes .= "col-md-6";
    }
    if (is_active_sidebar( 'sidebar-left' )) {
       //$classes .= " col-md-push-".($nbr_sidebars == 2 ? 3 : 4);
    }
    echo $classes;
}

function simple_boostrap_sidebar_left_classes() {
    $nbr_sidebars = (is_active_sidebar('sidebar-left') ? 1 : 0) + (is_active_sidebar('sidebar-right') ? 1 : 0);
    //echo 'col-md-'.($nbr_sidebars == 2 ? 3 : 4).' col-md-pull-'.($nbr_sidebars == 2 ? 6 : 8);
    echo 'col-md-'.($nbr_sidebars == 2 ? 3 : 4);
}

function simple_boostrap_sidebar_right_classes() {
    $nbr_sidebars = (is_active_sidebar('sidebar-left') ? 1 : 0) + (is_active_sidebar('sidebar-right') ? 1 : 0);
    echo 'col-md-'.($nbr_sidebars == 2 ? 3 : 4);
}

function get_shorten_text($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
}

function get_posts_count() {
    global $wp_query;
    return $wp_query->post_count;
}

function get_more_link(){
  global $post;
  return '&nbsp;[&nbsp;<a class="moretag" href="'. get_permalink($post_id) . '">více...</a>&nbsp;]';
}

function new_excerpt_more($more) {
  get_more_link();
}
add_filter('excerpt_more', 'new_excerpt_more');

function category_has_parent($catid){
    $category = get_category($catid);
    if ($category->category_parent > 0){
        return true;
    }
    return false;
}

function category_has_children($cat_id){
    $children = get_terms(
        'category',
        array( 'parent' => $cat_id, 'hide_empty' => false )
    );
    if ($children){
        return true;
    }
    return false;
}

add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_category() ) {
        $title = single_cat_title( '', false );
    }
    $title = str_replace("Archiv", "Archiv aktualit", $title);
    $title = str_replace(":", "", $title);
    return $title;
});

function theme_get_archives_link ( $link_html ) {
    global $wp;
    static $current_url;
    if ( empty( $current_url ) ) {
        $current_url = add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
    }
    if ( stristr( $current_url, 'page' ) !== false ) {
		$current_url = substr($current_url, 0, strrpos($current_url, 'page'));
    }
    if ( stristr( $link_html, $current_url ) !== false ) {
        $link_html = preg_replace( '/(<[^\s>]+)/', '\1 class="current-archive"', $link_html, 1 );
    }
    return $link_html;
}
add_filter('get_archives_link', 'theme_get_archives_link');

function display_slider_or_thumb($metaslider_id){
  global $post;
  if(is_category()){
    if(function_exists('z_taxonomy_image_url')){
      $url = z_taxonomy_image_url();
      echo '<div class="post_thumb_image white_delimiter">';
      echo '<img src="'.$url.'" class="img-responsive">';
      echo '</div>';
    }else{
      echo do_shortcode("[metaslider id=$metaslider_id]");
    }    
  }else{
    if ( has_post_thumbnail() ) {
      $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
      echo '<div class="post_thumb_image white_delimiter clearfix">';
      echo '<img src="'.$url.'" class="img-responsive col-xs-12">';
      echo '</div>';
    }else{
      echo do_shortcode("[metaslider id=$metaslider_id]");
    }
  }
}

function get_page_depth( $id=0, $depth=0 ) {
   global $post;
   
   if ( $id == 0 )
      $id = $post->ID;

   $page = get_post( $id );

   if ( !$page->post_parent ) {
      return $depth;
   }

   return get_page_depth( $page->post_parent, $depth+1 );
}


function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, get_more_link());
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    echo $the_excerpt;
}

/*
 * ****************************	 MIGRAČNÍ FUNKCE  ****************************************
 */
function insert_news(){
    global $wpdb;
    $news = $wpdb->get_results("SELECT * FROM `is_rss_clanky` WHERE id_kat=5 AND aktivni=1 ORDER BY datum_vydani DESC");
    foreach ( $news as $new ) {
	    $my_post = array(
		    'post_title'    => wp_strip_all_tags( $new->cs_titulek ),
		    'post_content'  => $new->cs_uvod.$new->cs_text,
		    'post_status'   => 'publish',
		    'guid'	    => site_url()."/".sanitize_title($subcat->cs_titulek),
		    'post_author'   => 1,
		    'post_type'     => 'post',
		    'post_date'     => $new->datum_vydani,
		    'post_date_gmt' => $new->datum_vydani,
		    'post_modified' => $new->datum_zmeny,
		    'post_modified_gmt'	=> $new->datum_zmeny,
		    'post_category' => array(2)
	    );
	    
	    //print_r($my_post);
	    $post_id = wp_insert_post( $my_post );
	    add_post_meta( $post_id, 'visits', $new->visit );
    }

}

function insert_l1_cats(){
  global $wpdb;
  $cats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE level='1' AND aktivni='1'");
  foreach ( $cats as $cat ) {
    $tax = "category";
    $parent_term = term_exists( $cat->cs_url, $tax ); // array is returned if taxonomy is given
    $parent_term_id = $parent_term['term_id']; // get numeric term id
			    
    wp_insert_term(
    $cat->cs_nazev, // the term 
    $tax, // the taxonomy
	    array(
		    'description'=>'',
		    'slug' => str_replace("/", "", $cat->cs_url),
		    'parent'=> ''
	    )
    );
    
    $wp_cat_parent_id = $wpdb->get_row("SELECT * FROM wp_terms WHERE slug='".str_replace("/", "", $cat->cs_url)."'");
    $my_post = array(
	    'post_title'    => wp_strip_all_tags( $cat->cs_nazev ),
	    'post_content'  => $cat->cs_popis,
	    'post_status'   => 'publish',
	    'post_author'   => 1,
	    'guid'	    => site_url()."/".sanitize_title($cat->cs_nazev),
	    'post_category' => array( $wp_cat_parent_id->term_id )
    );
    
    wp_insert_post( $my_post );
	  
  } 
}

function insert_l2_cats(){
  global $wpdb;    
  $cats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE level='2' AND aktivni='1'");  
  foreach ( $cats as $cat ) {
    $subcats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE id='$cat->id' AND aktivni='1' ORDER BY id ASC");
    foreach ( $subcats as $subcat ) {
      $url = explode('/', $subcat->cs_url);
      $wp_cat_parent_id = $wpdb->get_row("SELECT * FROM wp_terms WHERE slug='".$url[1]."' ORDER BY slug ASC");
      if(!empty($wp_cat_parent_id->term_id)){						
	$tax = "category";
	$parent_term = term_exists( $subcat->cs_url, $tax ); // array is returned if taxonomy is given
	$parent_term_id = $parent_term['term_id']; // get numeric term id
	
	wp_insert_term(
		$subcat->cs_nazev, // the term 
		$tax, // the taxonomy
		array(
			'description'=> '',
			'slug' => preg_replace("/[a-zA-Z0-9_-]/", "", $subcat->cs_url),
			'parent'=> $wp_cat_parent_id->term_id
		)
	);

	$wp_subcat_parent_id = $wpdb->get_row("SELECT * FROM wp_terms WHERE name='".$subcat->cs_nazev."'");
	$my_post = array(
		'post_title'    => wp_strip_all_tags( $subcat->cs_nazev ),
		'post_content'  => $subcat->cs_popis,
		'post_status'   => 'publish',
		'post_author'   => 1,
		'guid'	   	=> site_url()."/".sanitize_title($subcat->cs_nazev),
		'post_category' => array($wp_cat_parent_id->term_id, $wp_subcat_parent_id->term_id )
	);
	
	//print_r($my_post);
	wp_insert_post( $my_post );
      }
    }
  } 
}

function update_all_guids(){
  global $wpdb;
  $all_posts = $wpdb->get_results("SELECT id FROM wp_posts WHERE post_type='page' AND post_status='publish'");
  foreach ( $all_posts as $post ) {
    $wpdb->query("UPDATE wp_posts SET guid='".site_url()."/?p=".$post->id."' WHERE id='".$post->id."'");
  }
}

function insert_l1_pages(){
  global $wpdb;
  $cats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE level='1' AND aktivni='1'");
  foreach ( $cats as $cat ) {
    
    $my_post = array(
	    'post_title'    => wp_strip_all_tags( $cat->cs_nazev ),
	    'post_content'  => $cat->cs_popis,
	    'post_status'   => 'publish',
	    'post_author'   => 1,
	    'post_type'     => 'page',
	    'guid'	    => site_url()."/".sanitize_title($cat->cs_nazev)
    );
    
    wp_insert_post( $my_post );
	  
  } 
}

function insert_l2_pages(){
 global $wpdb;
 $cats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE level='2' AND aktivni='1'");  
  foreach ( $cats as $cat ) {
    $subcats = $wpdb->get_results("SELECT * FROM `is_rss_kategorie` WHERE id='$cat->id' AND aktivni='1' ORDER BY id ASC");
    foreach ( $subcats as $subcat ) {
	$url_array = explode("/", wp_strip_all_tags( $subcat->cs_url ));	
	//var_dump($url_array);
	$post_name = $url_array[1]=="pomozte-zachranit-zvirata" ? "jak-pomoci" : $url_array[1];
	$query = "SELECT * FROM wp_posts WHERE post_name='".$post_name."'";
	$wp_page_parent_id = $wpdb->get_row($query);
	if(count($wp_page_parent_id)>0){
	  $my_post = array(
		  'post_title'    => wp_strip_all_tags( $subcat->cs_nazev ),
		  'post_content'  => $subcat->cs_popis,
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'     => 'page',
		  'post_parent'   => $wp_page_parent_id->ID,
		  'guid'	   	=> site_url()."/".sanitize_title($subcat->cs_nazev),
		  'post_category' => array($wp_cat_parent_id->term_id, $wp_subcat_parent_id->term_id )
	  );
	  
	  //print_r($my_post);
	  //echo "<br><br><br>";
	  wp_insert_post( $my_post );    
	}
    }
  }
}
