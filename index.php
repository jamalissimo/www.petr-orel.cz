<?php /*
<?php get_header(); ?>

<div id="content" class="row">

	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">

		<?php 
		if (have_posts()) : 
			while (have_posts()) : the_post(); 
			simple_boostrap_display_page(true); 
			?>
		
		<?php endwhile; ?>	
		
		<?php //simple_boostrap_page_navi(); ?>	
		
		<?php else : ?>
		
		<article id="post-not-found" class="block">
		    <p><?php _e("No posts found.", "simple-bootstrap"); ?></p>
		</article>
		
		<?php endif; ?>

	</div>

	<?php get_sidebar("left"); ?>
	<?php get_sidebar("right"); ?>

</div>

<?php get_footer(); ?>
*/?>

<?php get_header(); ?>

<div id="content" class="row">

	<?php get_sidebar("category"); ?>

	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">
	
		<?php
		if (have_posts()){
		?>
		
		<?php
			simple_boostrap_display_page(false);
		?>		
		
		<?php //comments_template('',true); ?>
			
		<?php }else{ ?>
		
		<article id="post-not-found" class="block">
			<div class="article-header">
				<h2><?php _e(single_cat_title( '', false ), "simple-bootstrap"); ?></h2>
			</div>
			<p><?php _e("Nebyly nalezeny žádné příspěvky.", "simple-bootstrap"); ?></p>			
		</article>
		
		<?php } ?>

	</div>
	
	<?php get_sidebar("left"); ?>

</div>

<?php get_footer(); ?>