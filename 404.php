<?php get_header(); ?>

<div id="content" class="row">

	<div id="main" class="col-sm-12" role="main">

		<article id="post-not-found" class="block">
		
			<section class="post_content">
				
				<p>
					<h1>404</h1>
					<?php _e("Stránka, kterou hledáte, neexistuje, nebo byla přesunuta.", "simple-bootstrap"); ?>
					<br>
					<br>
					<?php _e("Můžete ji zkusit vyhledat pomocí tohoto formuláře", "simple-bootstrap"); ?>
				</p>
				<?php get_search_form(); ?>
		
			</section>
		
		</article>

	</div>

</div>

<?php get_footer(); ?>