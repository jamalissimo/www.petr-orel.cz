<?php get_header(); ?>

<div id="content" class="row">
	<?php get_sidebar("category"); ?>

	<div id="main" class="<?php simple_boostrap_main_classes(); ?>" role="main">

		<?php 		
		$categories = get_the_category();
		$category_id = $cat;
		$args = array(
			'posts_per_page'   => -1,
			'offset'           => 0,
			'category'         => $category_id,
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'post',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'	   => '',
			'post_status'      => 'publish',
			'suppress_filters' => true
		);
		
		$is_uvod = 0;
		$posts = get_posts($args);
		foreach ( $posts as $post ){
		  setup_postdata( $post );
		  if(in_category( 'Úvodní' )){
		    simple_boostrap_display_page_by_id($post->ID);
		    $is_uvod = 1;
		    break;
		  }
		}
		$category = get_category($category_id);		
		$count_posts = $category->category_count;
		/* debug
		echo "<br>";
		echo "<br>";
		echo "cat - $cat";
		echo "<br>";
		echo "count - $count_posts";
		echo "<br>";
		echo "is_uvod - $is_uvod";
		echo "<br>";
		*/
		if(!$is_uvod){
		  foreach ( $posts as $post ){
		    setup_postdata( $post );
		    $multiple = $count_posts>1 ? true : false;
		    simple_boostrap_display_page($multiple);
		  }		
		}
		
		wp_reset_postdata();

				/*
		?>		

		
		<article id="post-not-found" class="block">
			<div class="article-header">
				<h2><?php _e("Žádný obsah", "simple-bootstrap"); ?></h2>
			</div>
			<p><?php _e("Nebyly nalezeny žádné stránky.", "simple-bootstrap"); ?></p>			
		</article>
		


	</div>

	
	<?php*/ //get_sidebar("right"); ?>

</div>

<?php get_footer(); ?>
