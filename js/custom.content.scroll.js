jQuery( document ).ready(function() { 
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		console.log("Mobilní rozlišení");
	}else{
		if(!jQuery("#end_of_text").length){
			if(jQuery('#main p:nth-child(9)').length){
				jQuery('#main p:nth-child(9)').append("<h3 id='end_of_text' style='color:green'>----- zde budete pokračovat -----</h3>");
			}else{
				jQuery('#main p:nth-child(4)').append("<h3 id='end_of_text' style='color:green'>----- zde budete pokračovat -----</h3>");
			}
		}
		
		var main_div_waypoint = jQuery('.widget_nav_menu').waypoint(function(direction){
		if (direction === 'down') {
			console.log("Scrolling down");
			jQuery('.left-menu-images').css("display","none");   
			var y = jQuery(window).scrollTop(); 
			jQuery('#main').animate({width:'100%'}, 'slow');    
			//jQuery("html, body").animate({ scrollTop: y+jQuery( window ).height() }, 2000);
			jQuery('html, body').animate({
					scrollTop: jQuery("#end_of_text").offset().top - 150
			}, 2000);
		} else {
			console.log("Scrolling up");
			jQuery('#main').removeAttr('style');
			jQuery('.left-menu-images').removeAttr('style');
		}
		}, {
			offset: '-10%'
		}); 
	}
});