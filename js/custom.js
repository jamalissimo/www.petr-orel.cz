jQuery( document ).ready(function() {
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    console.log("Mobilní rozlišení");
  }else{
    var waypoints = jQuery('#footer-image').waypoint(function(direction){
      if (direction === 'down') {
	console.log("Scrolling down");
	jQuery('#footer-image').css("position","fixed");   
	jQuery('#footer-image').css({'height': '600px','width': '100%'});
      } else {
	console.log("Scrolling up");
	jQuery('#footer-image').css("position","relative");
	jQuery('#footer-image').css({'width': '0'});
      }
    }, {
      offset: '90%'
    });
  }
  
  jQuery("#nav_menu_category-aktuality #podstranky li a").each(function(){
    jQuery(this).prepend("Aktuality pro rok ");
  });

});

jQuery(function($) {
  $('img.bg-home-image').bind('mouseenter mouseleave', function() {
    $(this).attr({
	    src : $(this).attr('data-other-src'),
	    'data-other-src' : $(this).attr('src')
    })
  });
  
});
      
